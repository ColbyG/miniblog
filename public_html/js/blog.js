$(function() {
    var APPLICATION_ID = "1CC99CE5-7130-FD4C-FF6F-2F9578257500",
        SECRET_KEY = "E6B77AEE-CDFC-C009-FF95-646F209E9300",
        VERSION = "v1";
        
    Backendless.initApp(APPLICATION_ID, SECRET_KEY, VERSION);
   
    var postsCollection = Backendless.Persistence.of(Posts).find();
    console.log(postsCollection);
    
    var wrapper = {
        posts: postsCollection.data
    };
    
    Handlebars.registerHelper('format', function (time) {
       return moment(time).format("dddd, MMMM do YYYY");
    });
    
    var blogScript = $("#blogs-template").html();
    var blogTemplate = Handlebars.compile(blogScript);
    var blogHTML = blogTemplate(wrapper);

    $('.main-container').html(blogHTML);
 
});

function Posts(args){
    args = args || {};
    this.title = args.title || "";
    this.content = args.content || "";
    this.authorEmail = args.authorEmail ||"";
} 